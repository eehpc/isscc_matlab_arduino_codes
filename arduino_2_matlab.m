clear all
close all
clc
%%
delete(instrfind);
arduino=serial('COM5');
set(arduino,'BaudRate',9600);
fopen(arduino);
output = fscanf(arduino); % is Arduino done? Check and get data if so.
% get 200x2 arrays from MATLAB and save it into a mat file
array1 = zeros(200,1);
array2 = zeros(200,1);
for i=1:200 % get encoded arrays from arduino.
    data = fscanf(arduino);
    if(~isnan(data))
        commas = strfind(data,',');
        array1(i) = str2num(data(1:commas(1)-1));
        array2(i) = str2num(data(commas(1):end));
    end
end
disp('Measured for this trial');
pause(1)
fclose(arduino);
delete(arduino);
clear arduino;
%%
image = zeros(320,240);
M = cat(2,array1,array2);
[numRows,numCols] = size(M);

for row = 1:numRows
    X = M(row,1);
    Y = M(row,2);
    image(X,Y) = 1;
end
%% 
I = mat2gray(image);
I = flip(I,2);
I = imrotate(I,-90);
J = imresize(I,[28 28]);
J = J*2000;
subplot(3,1,1);
imshow(I);
subplot(3,1,2);
imshow(J);
imwrite(I,'zero_raw.png');
imwrite(J,'zero_pp.png');
K = zeros(28,28);
for i = 1:28
    for j = 1:28
        if(J(i,j) ~= 0)
            K(i,j) = 255;
        else 
            K(i,j) = 0;
        end
    end
end

subplot(3,1,3);
imshow(K);
