s = serial('COM3','BaudRate',9600);  % insert your serial%properties here
%sendData = 5;
% fopen(s);
% %fprintf(serialCom,'%i',sendData); %this will send 5 to the arduino
% x = fscanf(s);    %this will read response or use BytesAvailableFcn property of serial
% 
% if x == 5
%     fclose(s);
% else 
%     x = fscanf(s);
% end

s.BytesAvailableFcnCount = 40;
s.BytesAvailableFcnMode = 'byte';
s.BytesAvailableFcn = @instrcallback;
fopen(s);
record(s,'on');
out = fscanf(s);