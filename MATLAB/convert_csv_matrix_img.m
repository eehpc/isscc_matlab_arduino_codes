
image = zeros(320,240);
M = csvread('data.csv');
[numRows,numCols] = size(M);

for row = 1:numRows
    X = M(row,1);
    Y = M(row,2);
    image(X,Y) = 1;
end

I = mat2gray(image);
I = flip(I,2);
I = imrotate(I,-90);
J = imresize(I,[28 28]);
J = J*40;
subplot(2,1,1);
imshow(I);
subplot(2,1,2);
imshow(J);
imwrite(I,'zero_raw.png');
imwrite(J,'zero_pp.png');