close all
clear
clc
delete(instrfindall); %Solved unavailable port error
%----------------------Initializing vector----------------------------
Step = 784;
x = zeros(1,Step);
%--------------------------------------------------------------------------
%----------------------Initializing serial port----------------------------
Data = zeros(1, Step);

MyPort = serial('COM10','baudrate',9600,'databits',8, ...
    'parity','none','stopbits',1,'readasyncmode','continuous');
fopen(MyPort);
disp(MyPort)
disp('Start reading')
%----------------------Get some samples --------------------------------
MyPort.Timeout = 30;
for j = 1:10
    for i = 1:Step
    Data(i) = fread(MyPort,1, 'uint8');
    % b_x = dec2bin(x(i));
    end

    %%--------------------Neural Net processing starts here--------------------
    for i =1:28
        for j=1:28
            scaled_image(i,j) = Data(1,(j-1)+28*(i-1)+1); %%(i-1)+28*(j-1)+1
        end
    end

    I = mat2gray(scaled_image); 
    imshow(I);
end
%----------------------End reception & close serial port----------------
fclose(MyPort); %Disconnect port
delete(MyPort); %Remove port from memory
clear MyPort;   %Clean port object from workspace


tic
L1 = readNPY('L1_int.npy');
L2 = readNPY('L2_int.npy');


Fmap = transpose(double(L1))*transpose(double(Data));
shift=7;
% max(abs(Fmap))/2^shift;


Y_bitwidth=8;
Fmap=mod(Fmap, 2^33);
Fmap=floor(Fmap/(2^shift));
Fmap=mod(Fmap,2^(Y_bitwidth));
for i=1:512
 if Fmap(i)>2^(Y_bitwidth-1)
    Fmap(i) = Fmap(i)-2^Y_bitwidth-1;
 end
end
L1_out = Fmap;

Fmap = Fmap .* (Fmap>0);

Out =  transpose(double(L2))*double(Fmap);
shift=7;
Y_bitwidth=8;
Out=mod(Out, 2^33);
Out=floor(Out/(2^shift));
Out=mod(Out,2^(Y_bitwidth));
for i=1:10
 if Out(i)>2^(Y_bitwidth-1)
    Out(i) = Out(i)-2^Y_bitwidth-1;
 end
end

[argvalue, argmax] = max(Out);
Y_predicted = argmax - 1
toc