% clc
% clear all;
% data = xlsread('working_four.xlsx');
% data = 
% image=zeros(28,28);
% % for i = 1:149
% %     data(i,1) = data(i,1) / 10;
% %     data(i,2)= data(i,2) / 10;
% % end
% %%
% for i = 1:149
%     image(floor(data(i,2)),floor(data(i,1)))=255;
% end
%%
% image2=zeros(28,28);
% 
% thickness = 1;
% 
% for t = 1:thickness
%     for i = 2:27
%         for j = 2:27
%             if (image(i,j)>250)
%                image2(i,j)=255;
%                image2(i-1,j)=255;
%                image2(i-1,j+1)=255;
%                image2(i,j+1)=255;
%                image2(i+1,j+1)=255;
%                image2(i+1,j)=255;
%                image2(i+1,j-1)=255;
%                image2(i,j-1)=255;
%                image2(i-1,j-1)=255;
%             end
%         end
%     end
% image = image2;   
% end


                
% scaled_image2=zeros(28,28);
% for i =1:28
%     for j=1:28
%         scaled_image(i,j) = image(i, j);
%     end
% end


% I = mat2gray(image);
% imshow(I);
%%
clc
clear all;
scaled_image2=zeros(28,28);
data = csvread('data_1.csv');
% data = data;
for i =1:28
    for j=1:28
        scaled_image(i,j) = data(1,(j-1)+28*(i-1)+1); %%(i-1)+28*(j-1)+1
    end
end

I = mat2gray(scaled_image); 
% subplot(2,1,1);
imshow(I);

L1 = readNPY('L1_int.npy');
L2 = readNPY('L2_int.npy');


% [indimage, rgbmap] = imread('sample.bmp');
% indimage = (indimage);
% for row = 1:28
%  for col = 1:28
% %     X(28*(row-1)+(col-1)+1) = indimage(row,col,1);
%     X(28*(row-1)+(col-1)+1) = I(row,col);
%  end
% end
% % Y = X';
% 
% % X= mnist2(1735,:);
% image = zeros(28,28);
% for row = 1:28
%  for col = 1:28
%     image(row,col) = X(28*(row-1)+(col-1)+1);
%  end
% end
% J = mat2gray(image);
% subplot(2,1,2);
% imshow(J);


% Fmap = double(X)*double(L1);
% Out =  double(Fmap)*double(L2);
% [argvalue, argmax] = max(Out);
% Y_predicted = argmax - 1


Fmap = transpose(double(L1))*transpose(double(data));
shift=7;
max(abs(Fmap))/2^shift;


Y_bitwidth=8;
Fmap=mod(Fmap, 2^33);
Fmap=floor(Fmap/(2^shift));
Fmap=mod(Fmap,2^(Y_bitwidth));
for i=1:512
 if Fmap(i)>2^(Y_bitwidth-1)
    Fmap(i) = Fmap(i)-2^Y_bitwidth-1;
 end
end
L1_out = Fmap;

Fmap = Fmap .* (Fmap>0);

Out =  transpose(double(L2))*double(Fmap);
% Out = floor(Out/2^7);
% means the second shift is 7

shift=7;
Y_bitwidth=8;
Out=mod(Out, 2^33);
Out=floor(Out/(2^shift));
Out=mod(Out,2^(Y_bitwidth));
for i=1:10
 if Out(i)>2^(Y_bitwidth-1)
    Out(i) = Out(i)-2^Y_bitwidth-1;
 end
end


[argvalue, argmax] = max(Out);
Y_predicted = argmax - 1