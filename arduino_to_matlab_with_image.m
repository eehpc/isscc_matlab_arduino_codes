clear all
close all
clc
%%
delete(instrfind);
arduino=serial('COM5');
set(arduino,'BaudRate',9600);
fopen(arduino);
output = fscanf(arduino); % is Arduino done? Check and get data if so.
% get 200x2 arrays from MATLAB and save it into a mat file
array1 = zeros(784,1);
%     for l=1:784 % get encoded arrays from arduino.
        data = fscanf(arduino);
        if(~isnan(data))
%             commas = strfind(data,',');
            for i =1:784
                    array1(i,1) = str2num(data(1:8));
            end
        end
%     end
    disp('Measured for this trial');
pause(1)
fclose(arduino);
delete(arduino);
clear arduino;
%%
% delete(instrfind);
% serialCom = serial('COM4','BaudRate',9600);  % insert your serial
% fopen(serialCom);
% % tic
% for i = 1 : 784
%     data_hex = array1(i,1);
% %     data_to_send = hex2dec(data_hex);
%     fprintf(serialCom,'%c',data_hex);
% end
% % toc
% fclose(serialCom);
% string = 'done';
