clc;
clear all;

[indimage, rgbmap] = imread('sample.bmp');
indimage = double(indimage);
for row = 1:28
 for col = 1:28
    X(28*(row-1)+(col-1)+1) = indimage(row,col,1);
 end
end 
Y = X';

% X= mnist2(1735,:);
image = zeros(28,28);
for row = 1:28
 for col = 1:28
    image(row,col) = X(28*(row-1)+(col-1)+1);
 end
end
I = mat2gray(image);
imshow(I);

delete(instrfind);
serialCom = serial('COM4','BaudRate',9600);  % insert your serial
fopen(serialCom);
% tic
for i = 1 : 784
    data_hex = Y(i,1);
%     data_to_send = hex2dec(data_hex);
    fprintf(serialCom,'%d',data_hex);
end
% toc
fclose(serialCom);
string = 'done';
