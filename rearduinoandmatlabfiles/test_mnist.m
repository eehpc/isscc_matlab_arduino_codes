% clear all;
% clc;

L1 = readNPY('L1_int.npy');
L2 = readNPY('L2_int.npy');


% mnist = load('mnist.mat');
% mnist2 = readNPY('X_test_150.npy');
% mnist2 = 63.5*(mnist2+1);
% index = 1411;
index = 1204;

% X = int8(mnist.testX(index,:));
% Y = int8(mnist.testY(index))

% x = zeros(784);
% x = csvread('data_8_j_i.csv');
% X=x(1:784);
[indimage, rgbmap] = imread('sample.bmp');
indimage = (indimage);
for row = 1:28
 for col = 1:28
%     X(28*(row-1)+(col-1)+1) = indimage(row,col,1);
    X(28*(row-1)+(col-1)+1) = I(row,col);
 end
end
Y = X';

% X= mnist2(1735,:);
image = zeros(28,28);
for row = 1:28
 for col = 1:28
    image(row,col) = X(28*(row-1)+(col-1)+1);
 end
end
I = mat2gray(image);
imshow(I);


% Fmap = double(X)*double(L1);
% Out =  double(Fmap)*double(L2);
% [argvalue, argmax] = max(Out);
% Y_predicted = argmax - 1


Fmap = transpose(double(L1))*transpose(double(X));
shift=7;
max(abs(Fmap))/2^shift;


Y_bitwidth=8;
Fmap=mod(Fmap, 2^33);
Fmap=floor(Fmap/(2^shift));
Fmap=mod(Fmap,2^(Y_bitwidth));
for i=1:512
 if Fmap(i)>2^(Y_bitwidth-1)
    Fmap(i) = Fmap(i)-2^Y_bitwidth-1;
 end
end
L1_out = Fmap;

Fmap = Fmap .* (Fmap>0);

Out =  transpose(double(L2))*double(Fmap);
% Out = floor(Out/2^7);
% means the second shift is 7

shift=7;
Y_bitwidth=8;
Out=mod(Out, 2^33);
Out=floor(Out/(2^shift));
Out=mod(Out,2^(Y_bitwidth));
for i=1:10
 if Out(i)>2^(Y_bitwidth-1)
    Out(i) = Out(i)-2^Y_bitwidth-1;
 end
end


[argvalue, argmax] = max(Out);
Y_predicted = argmax - 1
