clc
clear all;
data = csvread('data.csv');
image=zeros(320,280);
for i = 1:99
    image(data(i,1),data(i,2))=255;
end

image2=zeros(320,280);

thickness=8;

for t = 1:thickness
    for i = 2:319
        for j = 2:279
            if (image(i,j)>250)
               image2(i,j)=255;
               image2(i-1,j)=255;
               image2(i-1,j+1)=255;
               image2(i,j+1)=255;
               image2(i+1,j+1)=255;
               image2(i+1,j)=255;
               image2(i+1,j-1)=255;
               image2(i,j-1)=255;
               image2(i-1,j-1)=255;
            end
        end
    end
image = image2;   
end


                
scaled_image=zeros(28,28);
for i =1:28
    for j=1:28
        scaled_image(i,j) = image2(11*i, 10*j);
    end
end



I = mat2gray(scaled_image);
imshow(I);
