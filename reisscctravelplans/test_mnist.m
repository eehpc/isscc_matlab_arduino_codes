clear all;
clc;
L1 = readNPY('L1_int.npy');
L2 = readNPY('L2_int.npy');
mnist = load('mnist.mat');

index = 1353;
X = int8(mnist.testX(index,:));
Y = int8(mnist.testY(index))

Fmap = double(X)*double(L1);
Out =  double(Fmap)*double(L2);
[argvalue, argmax] = max(Out);
Y_predicted = argmax - 1