#include <Elegoo_GFX.h>    // Core graphics library
#include <Elegoo_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>

#if defined(__SAM3X8E__)
    #undef __FlashStringHelper::F(string_literal)
    #define F(string_literal) string_literal
#endif



#define YP A3  // must be an analog pin, use "An" notation!
#define XM A2  // must be an analog pin, use "An" notation!
#define YM 9   // can be a digital pin
#define XP 8   // can be a digital pin
/*
#define TS_MINX 50
#define TS_MAXX 920

#define TS_MINY 100
#define TS_MAXY 940
*/
//Touch For New ILI9341 TP
#define TS_MINX 120
#define TS_MAXX 900

#define TS_MINY 70
#define TS_MAXY 920

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
// optional
#define LCD_RESET A4

// Assign human-readable names to some common 16-bit color values:
#define  BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define MINPRESSURE 10
#define MAXPRESSURE 1000

Elegoo_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

#define BOXSIZE 40
#define PENRADIUS 4
#define counter 200
//byte reset, tx;

byte dont_reset = 0;
float A,B;
short data[counter][2];
byte image[28][28];
int m = 0;
int i,j;
void setup(void) {
  Serial.begin(9600);
//  Serial.println(F("Paint!"));
  
  tft.reset();
  
  uint16_t identifier = tft.readID();
  if(identifier == 0x9325) {
    Serial.println(F("Found ILI9325 LCD driver"));
  } else if(identifier == 0x9328) {
    Serial.println(F("Found ILI9328 LCD driver"));
  } else if(identifier == 0x4535) {
    Serial.println(F("Found LGDP4535 LCD driver"));
  }else if(identifier == 0x7575) {
    Serial.println(F("Found HX8347G LCD driver"));
  } else if(identifier == 0x9341) {
    Serial.println(F("Found ILI9341 LCD driver"));
  } else if(identifier == 0x8357) {
    Serial.println(F("Found HX8357D LCD driver"));
  } else if(identifier==0x0101)
  {     
      identifier=0x9341;
//       Serial.println(F("Found 0x9341 LCD driver"));
  }else {
    Serial.print(F("Unknown LCD driver chip: "));
    Serial.println(identifier, HEX);
    Serial.println(F("If using the Elegoo 2.8\" TFT Arduino shield, the line:"));
    Serial.println(F("  #define USE_Elegoo_SHIELD_PINOUT"));
    Serial.println(F("should appear in the library header (Elegoo_TFT.h)."));
    Serial.println(F("If using the breakout board, it should NOT be #defined!"));
    Serial.println(F("Also if using the breakout, double-check that all wiring"));
    Serial.println(F("matches the tutorial."));
    identifier=0x9341;
   
  }

  tft.begin(identifier);
  tft.setRotation(2);
  tft.fillScreen(BLACK);
  
  tft.drawRect(0, 0, 240, 245, WHITE);
  tft.fillRect(0, 282, 119, 320, RED);
  tft.fillRect(119, 282, 240, 320, GREEN);
  tft.setCursor(25, 295);
  tft.setTextColor(WHITE);  tft.setTextSize(2);
  tft.println("RESET");

  tft.setCursor(150, 295);
  tft.setTextColor(WHITE);  tft.setTextSize(2);
  tft.println("EVAL");
//  dont_reset = 1;
  pinMode(13, OUTPUT);
  
//  tft.setCursor(66, 255);
//  tft.setTextColor(CYAN);  tft.setTextSize(2);
//  tft.println("Enter 0-9");
}

void loop()
{
  digitalWrite(13, HIGH);
  TSPoint p = ts.getPoint();
  digitalWrite(13, LOW);
  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);
  //pinMode(YM, OUTPUT);

  if (p.z > MINPRESSURE && p.z < MAXPRESSURE) {

  p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0);
  p.y = (tft.height()-map(p.y, TS_MINY, TS_MAXY, tft.height(), 0));


  //PENRADIUS
  if ((p.y) < 240 && ((p.y+PENRADIUS) < tft.height()) && m < counter) {
      tft.fillCircle(p.x, p.y, PENRADIUS, WHITE);
          data[m][0] = p.x;
          data[m][1] = p.y;
//          Serial.print(data[m][0]);
//          Serial.print(",");
//          Serial.print(data[m][1]);
//          Serial.println(",");
          m = m + 1;
        }
  else{
//    dont_reset == 0;
//    tft.fillRect(0, 254, 240, 279, BLACK);
    tft.setCursor(100, 255);
    tft.setTextColor(RED);  tft.setTextSize(2);
    tft.println("DONE");
  }


     if(p.y > 281) {
        if(p.x < 120) {
//            tft.drawRect(0, 282, 119, 320, RED);
//            tft.fillRect(1, 283, 118, 319, CYAN);
//            tft.fillRect(119, 282, 240, 320, GREEN);
            for(i=0; i<28; i=i+1){
              for(j=0; j<28; j=j+1){
                image[j][i] = 0;
              }
            }
            m = 0;
            dont_reset = 0;
            tft.fillRect(0, 0, 240, 279, BLACK);
            tft.drawRect(0, 0, 240, 245, WHITE);

//            tft.setCursor(66, 255);
//            tft.setTextColor(CYAN);  tft.setTextSize(2);
//            tft.println("Enter 0-9");
          }
        else if(p.x > 120){
          if(dont_reset < 1){
              for(i=0; i<counter; i++)
                {
                A = floor((data[i][0])/8.8); 
                B = floor((data[i][1])/8.8);             
                data[i][0] = (int)A;
                data[i][1] = (int)B;
                }
              for(i=0; i<counter; i++)
                {
                image[data[i][0]][data[i][1]] = 127;
                }
                
              for(i=0+1; i<28-1; i++)
                {
                for(j=0+1; j<28-1; j++)
                  {
                    if(image[i][j]>100)
                      {
                      image[i-1][j]=127;
                      image[i][j-1]=127;
                      image[i-1][j-1]=127;
                      }
                  }
                }
              for(i=0; i<4; i++) {
                for(j=0; j<4; j++){
                  image[i][j] = 0;
                }
              }
          }
            dont_reset += 1; 
            for(i=0; i<28; i=i+1){
              for(j=0; j<28; j=j+1){
                Serial.write(image[j][i]);
//                Serial.print(",");

              }
            }
        }
  
    }
  }
}

   
