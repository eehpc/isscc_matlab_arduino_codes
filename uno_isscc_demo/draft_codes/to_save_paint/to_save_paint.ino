#include <Elegoo_GFX.h>    // Core graphics library
#include <Elegoo_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>

#if defined(__SAM3X8E__)
    #undef __FlashStringHelper::F(string_literal)
    #define F(string_literal) string_literal
#endif



#define YP A3  // must be an analog pin, use "An" notation!
#define XM A2  // must be an analog pin, use "An" notation!
#define YM 9   // can be a digital pin
#define XP 8   // can be a digital pin
/*
#define TS_MINX 50
#define TS_MAXX 920

#define TS_MINY 100
#define TS_MAXY 940
*/
//Touch For New ILI9341 TP
#define TS_MINX 120
#define TS_MAXX 900

#define TS_MINY 70
#define TS_MAXY 920

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

#define LCD_CS A3
#define LCD_CD A2
#define LCD_WR A1
#define LCD_RD A0
// optional
#define LCD_RESET A4

// Assign human-readable names to some common 16-bit color values:
#define  BLACK   0x0000
//#define BLUE    0x001F
//#define RED     0xF800
//#define GREEN   0x07E0
//#define CYAN    0x07FF
//#define MAGENTA 0xF81F
//#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define MINPRESSURE 10
#define MAXPRESSURE 1000

Elegoo_TFTLCD tft(LCD_CS, LCD_CD, LCD_WR, LCD_RD, LCD_RESET);

#define BOXSIZE 40
#define PENRADIUS 4
int oldcolor, currentcolor, old_x, old_y;

float A,B;
short data[200][2];
byte image[28][28];
int m = 0, n = 0;
int incomingByte = 0;
int i,j;
void setup(void) {
  Serial.begin(9600);
//  Serial.println(F("Paint!"));
  
  tft.reset();
  
  uint16_t identifier = tft.readID();
  if(identifier == 0x9325) {
    Serial.println(F("Found ILI9325 LCD driver"));
  } else if(identifier == 0x9328) {
    Serial.println(F("Found ILI9328 LCD driver"));
  } else if(identifier == 0x4535) {
    Serial.println(F("Found LGDP4535 LCD driver"));
  }else if(identifier == 0x7575) {
    Serial.println(F("Found HX8347G LCD driver"));
  } else if(identifier == 0x9341) {
    Serial.println(F("Found ILI9341 LCD driver"));
  } else if(identifier == 0x8357) {
    Serial.println(F("Found HX8357D LCD driver"));
  } else if(identifier==0x0101)
  {     
      identifier=0x9341;
//       Serial.println(F("Found 0x9341 LCD driver"));
  }else {
    Serial.print(F("Unknown LCD driver chip: "));
    Serial.println(identifier, HEX);
    Serial.println(F("If using the Elegoo 2.8\" TFT Arduino shield, the line:"));
    Serial.println(F("  #define USE_Elegoo_SHIELD_PINOUT"));
    Serial.println(F("should appear in the library header (Elegoo_TFT.h)."));
    Serial.println(F("If using the breakout board, it should NOT be #defined!"));
    Serial.println(F("Also if using the breakout, double-check that all wiring"));
    Serial.println(F("matches the tutorial."));
    identifier=0x9341;
   
  }

  tft.begin(identifier);
  tft.setRotation(2);
  tft.fillScreen(BLACK);
 
  pinMode(13, OUTPUT);
}

void loop()
{
  digitalWrite(13, HIGH);
  TSPoint p = ts.getPoint();
  digitalWrite(13, LOW);
  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);
  //pinMode(YM, OUTPUT);

  if (p.z > MINPRESSURE && p.z < MAXPRESSURE) {

  p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0);
  p.y = (tft.height()-map(p.y, TS_MINY, TS_MAXY, tft.height(), 0));

  Serial.print(p.x);
  Serial.print(",");  
  Serial.print(p.y);
  Serial.println(",");
  
  if ((p.y-PENRADIUS) && ((p.y+PENRADIUS) < tft.height() && m < 200)) {
      tft.fillCircle(p.x, p.y, PENRADIUS, WHITE);
       
          data[m][0] = p.x;
          data[m][1] = p.y;
          m = m + 1;
  }
        else {
          for(i=0; i<200; ++i)
          {
            A = floor((data[i][0])/9); 
            B = floor((data[i][1])/9);
            data[i][0] = (int)A;
            data[i][1] = (int)B;
//            Serial.print(data[i][0]);
//            Serial.print(",");
//            Serial.println(data[i][1]);
          }

          for(i=0; i<200; ++i)
          {
                image[data[i][0]][data[i][1]]=127;
          }

          for(i=0+1; i<28-1; ++i)
          {
          for(j=0+1; j<28-1; ++j)
          {
           if(image[i][j]>1)
           {
               image[i-1][j]=127;

               image[i][j-1]=127;
               image[i-1][j-1]=127;
           }
          }
          }
          for(i=0; i<4; i++) {
            for(j=0; j<4; j++){
              image[i][j] = 0;
            }
          }
          

          for(i=0; i<28; i=i+1){
            for(j=0; j<28; j=j+1){
//                Serial.print("1");
              Serial.write(image[j][i]);
//              Serial.print(",");
//              delay(5);
//              Serial.print(",");  
              }
            }
//            Serial.print(i*j);

//          Serial.print("------------------------------");
          }
          
      


          

          
         }       
  }
  


  
    if(Serial.available() > 0){
      incomingByte = Serial.read();
      if(incomingByte == 's'){
        for(i=0; i<28; i=i+1){
            for(j=0; j<28; j=j+1){
//                Serial.print("1");
              Serial.print(image[j][i]);
              delay(100);
//              Serial.print(",");  
              }
//            }
//            Serial.println(image);
//        for(i=0;i<200;i=i+1) {
//          Serial.print(data[i][1]);
//          Serial.print(",");
//          Serial.print(data[i][2]);
//          Serial.print("\n");
//          }
      }
    }
    }
  }

   
